\documentclass[10pt]{letter}
\usepackage{UPS_letterhead,xcolor,mhchem,mathpazo,ragged2e,hyperref}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{HTML}{009900}


\begin{document}

\begin{letter}%
{To the Editors of the Journal of Chemical Physics}

\opening{Dear Editors,}

\justifying
Please find attached a revised version of the manuscript entitled 
\begin{quote}
\textit{``Dynamical Correction to the Bethe-Salpeter Equation Beyond the Plasmon-Pole Approximation''}.
\end{quote}
We thank the reviewers for their constructive comments.
Our detailed responses to their comments can be found below.
For convenience, changes are highlighted in red in the revised version of the manuscript. 

We look forward to hearing from you.

\closing{Sincerely, the authors.}

%%% REVIEWER 1 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#1}

\begin{itemize}

	\item 
	{The manuscript by Loos and Blase discusses high-quality excited-state 
calculations for small prototypical molecules. The authors apply 
the GW-BSE approach which, starting from HF orbitals and energy levels, 
constructs single-particle and charge-neutral excitations via the 
equation of motion of corresponding Green functions. While the group 
has already published several papers in that direction, their present 
focus is on the dynamics of the dielectric screening, in particular 
for the Bethe-Salpeter equation (BSE). 

The merit of the present paper is the comprehensive discussion of 
quite a large number of transitions in prototypical molecules, thus 
providing benchmark data for others. The data appear to be of very 
high quality. 

I believe the paper is worth publishing.}
	\\
	\alert{We thank the reviewer for supporting the publication of the present manuscript.}
 
	\item
{Nonetheless I have a general comment: 
Concerning the significance of screening dynamics, I am not really sure 
if the authors go beyond what others have already done. They go beyond 
plasmon-pole modelling, which is a very good concept since plasmon-pole 
models have always been questioned concerning their reliability - however, 
the authors do not compare their results with data obtained from 
plasmon-pole modelling. If they have such data readily available, 
they might want to include them in the manuscript.}
	\\
	\alert{The reviewer is correct. 
	We go beyond Strinati's seminal work which was restricted to the TDA, and we attempt to provide to the reader more details about the derivation (see Eqs.~(12)-(18) and the Appendices), but indeed our dynamical BSE equations are eventually identical to the one provided by Rohlfing and coworkers. 
	This is now restated again after Eq.~(21) where we write: 
	``and triplet excited states (respectively). 
	This equation is identical to the one presented by Rohlfing and coworkers.'' 
	Further, the possible divergence of the ``beyond-TDA'' corrections forces us, as our colleagues Rohlfing and coworkers or Romaniello and coworkers, to stick to the TDA (this fact was not explicitly discussed in previous papers and we devote a small paragraph to that point). 
	We really feel however that performing reference calculations on a large set of transitions, with well defined and standard basis sets, without the plasmon-pole approximation, and with comparison to reference calculations rather than experimental data, is an important step to better assess the merits of this ``simpler'' dynamical approach, before embarking into more sophisticated treatments. 
	We did not implement the plasmon-pole approximation to perform a comparison with our exact (within RPA) dynamics for the reference molecules we study. 
	There are actually many plasmon-pole models (Rohlfing-Kr\"uger-Pollman, Hybersten-Louie, Godby-Needs, etc.) and we feel that it is a full fledge study outside the scope of the present study to assess the merits of these various models. 
	We hope that the availability of reference calculations that we provide may serve as a solid starting point to perform such studies.}
	
	\item
{Concerning the other aspect, i.e. renormalization of the BSE 
excitation energies due to dynamics, the authors seem to have done 
what others did before, again in a perturbative manner (see Eq. (42)), 
but no more, at least not in this 
manuscript. Of course, their method will allow for highly interesting 
effects in the future (multiple solutions, re-structuring the composition 
of excitations, satellite structure, etc.), 
but not in this paper. Or have I misunderstood something? 
In any case, I believe the authors should more clearly state how their 
(present) concept relates to previous work in the literature, and 
more clearly distinguish between present data and future perspectives. 
It's all in the manuscript, but somehow 'distributed', and it would 
be helpful to have all this in one paragraph. }
	\\
	\alert{The reviewer is right. 
	The present perturbative treatment cannot access additional excitations, and we are definitely interested in pursuing in this direction in the near future.
	This is clearly stated in the original version of our manuscript near the end of the Introduction:
	``It is important to note that, although all the studies mentioned above are clearly going beyond the static approximation of BSE, they are not able to recover additional excitations as the perturbative treatment accounts for dynamical effects only on excitations already present in the static limit. However, it does permit to recover, for transitions with a dominant single-excitation character, additional relaxation effects coming from higher excitations.''
	To answer the reviewer's comment we have added a new paragraph to the concluding section, which reads: ``Dynamical corrections have been calculated using a renormalized first-order perturbative correction to the static BSE excitation energies following the work of Rohlfing and coworkers.
Note that, although the present study goes beyond the static approximation of BSE, we do not recover additional excitations as the perturbative treatment accounts for dynamical effects only on excitations already present in the static limit. 
However, we hope to report results on a genuine dynamical approach in the near future in order to access double excitations within the BSE formalism.''
We have also added in Sec.~II.E. the following statement: ``As mentioned in Sec.~I, the present perturbative scheme does not allow to access double excitations as one loses the dynamical nature of the screening by applying perturbation theory. 
We hope to report a genuine dynamical treatment of the BSE in a forthcoming work.''}

	\item 
	{The authors emphasize that their $W$ is evaluated within RPA for 
many more than just one frequency (avoiding plasmon-pole modelling etc.). 
I assume that they employ the same 'high-quality' screening dynamics for 
evaluating their GW energy levels, correct? They should clearly state 
that somewhere (unless I missed it). }
	\\
	\alert{Yes, this is correct. We have clarified this point above Eq.~(25).}

	\item 
	{The authors use two different types of basis sets, cc-pVXZ and 
aux-cc-pVXZ, both of which can be improved towards convergence by 
choosing X=D, T, Q (etc.). However, Tab. I shows that the nitrogen $GW$ 
quasiparticle gap does not converge towards the same value for the two 
families (cc-pVQZ: 20.05 eV, aux-cc-pQVZ: 19.00 eV). 
This seems to be a substantial difference. Why is that? }
	\\
	\alert{This is completely normal. 
	To reach the complete basis set limit, one must have a basis set which is angularly complete as well as radially complete.
	When X goes up, one improves the angular completeness of the basis set, while diffuse functions take care of the radial completeness.
	For excited states and HOMO-LUMO gaps, because of the special importance of diffuse functions in these cases, the limit that one reaches with these two families of basis functions is indeed different. 
	We refer the reviewer to [J. Chem. Phys. 151, 144118 (2019)] where the present observation is clearly illustrated.
	We have mentioned this fact in the revised version near the beginning of the Results section.}

	\item 
	{Near the end of Sec. IV the authors state that CT or Rydberg states 
observe weak dTDA effects because the single-particle overlap (e.g., 
between HOMO and LUMO) is small. I agree, but it should also be 
mentioned that the excitonic binding itself is also small, for the 
same reason. 
}
	\\
	\alert{Thank you for pointing that out.
	We have mentioned this in the revised version of the manuscript.}

\end{itemize}

%%% REVIEWER 2 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#2}

\begin{itemize}
	
	\item 
	{In the context of computational spectroscopy, dynamical corrections to the Bethe-Salpeter equation (BSE) are investigated for small organic molecules. The authors present their implementation of dynamical corrections that have been derived before. Their work goes beyond previous work in that it does not use the plasmon pole approximation for the screened Coulomb interaction and is thus potentially more accurate. The dynamical BSE calculations are benchmarked for a test set of molecules for which high-level coupled cluster reference calculations are available. 

The work is rigorous, carefully done and well presented. The dynamical corrections can be quiet sizeable and improve the BSE results considerably. With the full screened Coulomb interaction, the corrections become larger than previously reported for the plasmon pole model. Still, the dynamical corrections do not add significant computational effort, which makes the BSE scheme highly competitive for optical excitations of weakly to moderately correlated molecules. The methodology and the results will be highly interesting for the core readership of The Journal of Chemical Physics and I recommend publication after the small comments below have been addressed.}
	\\
	\alert{We would like to thank the reviewer for his/her kinds comments and recommending publication of the present manuscript.}
	
	\item 
	{Throughout the manuscript I kept wondering, if the dynamical corrections had already been derived before. 
	This is not particularly clear and I encourage the authors to clarify this. 
	For example, in Section E are equations 32 to 42 new or were they already derived by Rohlfing and others? }
	\\
	\alert{
	We have clarified this point in the reply to Reviewer \#1 (see above). 
	Equations 32 to 42 aren't new.
	Originally, the dynamical correction has been derived by Strinati [see Ref.~(2) for a detailed derivation] but we have gone further by deriving also the anti-resonant term (see Sec. II.D.).
	We have clarified this point in two places in the revised manuscript (Introduction and Conclusion).}

	\item 
	{In the introduction, the authors advocate the dynamical BSE corrections for double excitations. 
	Yet, in the results section of the manuscript, no double excitations are reported. All result tables include a column for $\Omega_S^\text{stat}$, which I understand is from a standard, static BSE calculation. 
	For double excitations, however, I expect static BSE to not give a solution (or state) at all. 
	Thus, the corresponding table entry would be blank. In fact, in the methodological write up, the authors point to these additional solutions in several places, e.g. just after eq. 32 "Note that due to its non-linear nature, eq. 32 may provide more than one solution for each value of S." 

	So I wonder, does the perturbative approach taken for the dynamical corrections provide access to double excitations? 
	In eq. 42, which is the final expression, all quantities carry an S index. This implies that dynamical corrections are only calculated for states that are already part of the static solution and no new states can be found with this approach. What is the potential then for applying dynamical corrections to double excitations? This should be clarified. }
	\\
	\alert{This comment is similar to one of the comment of Reviewer \#1.	
	The present perturbative treatment cannot access additional excitations, and we are definitely interested in pursuing in this direction in the near future.
	This is clearly stated in the original version of our manuscript near the end of the Introduction:
	``It is important to note that, although all the studies mentioned above are clearly going beyond the static approximation of BSE, they are not able to recover additional excitations as the perturbative treatment accounts for dynamical effects only on excitations already present in the static limit. However, it does permit to recover, for transitions with a dominant single-excitation character, additional relaxation effects coming from higher excitations.''
	Nonetheless, to make it extra clear, we have added a new paragraph to the concluding section, which reads: ``Dynamical corrections have been calculated using a renormalized first-order perturbative correction to the static BSE excitation energies following the work of Rohlfing and coworkers.
Note that, although the present study goes beyond the static approximation of BSE, we do not recover additional excitations as the perturbative treatment accounts for dynamical effects only on excitations already present in the static limit. 
However, we hope to report results on a genuine dynamical approach in the near future in order to access double excitations within the BSE formalism.''
We have also added in Sec.~II.E. the following statement: ``As mentioned in Sec.~I, the present perturbative scheme does not allow to access double excitations as one loses the dynamical nature of the screening by applying perturbation theory. 
We hope to report a genuine dynamical treatment of the BSE in a forthcoming work.''}

	\item 
	{Equation 41 contains a derivative of $A^{(1)}(\Omega_S)$ with respect to $\Omega_S$. 
	Is this derivative easy to compute? 
	Is $A^{(1)}(\Omega_S)$ available in analytic form or is the derivative performed numerically? }
	\\
	\alert{Yes, this derivative is straightforward to compute analytically, and it is computed at no extra cost basically (as mentioned in the original manuscript, see Sec.~II.E.).
	The situation is very similar to the computation of the derivative of the $GW$ self-energy $\Sigma$ which is involved in the calculation of the spectral weight $Z$.}

	\item 
	{Following up on my previous point, why does equation 40 have to be renormalised? 
	It could be solve iteratively, as is done for the quasiparticle equation in $GW$. 
	Then the aforementioned derivative would not have to be calculated. }
	\\
	\alert{Yes, the referee is right. There are two possibilities to solve this equation: renormalization or self-consistency.
	As mentioned on page 6 of our original manuscript: ``Moreover, we have observed that an iterative, self-consistent resolution [where the dynamically-corrected excitation energies are re-injected in Eq. (39)] yields basically the same results as its (cheaper) renormalized version.''
	In other words, we have tested both strategies and they basically yield identical results.
	However, as mentioned above, the renormalized version is much cheaper as the derivative of the screening is basically free and one does not have to recompute several quantities that must be updated in the self-consistent version.}

	\item 
	{First paragraph of the introduction: "In recent years, it has been shown to be a valuable tool for computational chemists with a large number os systematic benchmark studies on large families of molecular systems appearing the literature [11-20] (see Ref. 21 for a recent review)." Maybe one could reference also the following, recent all-electron BSE implementation and benchmark study here: C. Liu, J. Kloppenburg, Y. Yao, X. Ren, H. Appel, Y. Kanai, and V. Blum, J. Chem. Phys. 152, 044105 (2020) }
	\\
	\alert{The reference has been added in due place.}

	\item 
	{Results for N2 are reported in Table II. 
	Not far off the equilibrium bond length, N2 exhibits a conical intersection in its $^5 \Pi_u$ and $^1\Delta_u$ states. 
	These should also be visible in the optical transition energies. 
	Such a conical intersection would be a good test for a theory that goes beyond standard BSE and can tackle more correlated systems, as recently demonstrated, for example, for dynamical configuration interaction (DCI) theory, which also includes GW and BSE elements (see e.g. M. Dvorak, D. Golze, and P. Rinke, Physical Review Materials 3, 070801(R) (2019)). }
	\\
	\alert{This is an interesting comment. Although outside the scope of the present study, we hope to be able to check the existence of this conical intersection at the dynamical BSE level in the near future while working with a fully dynamical scheme (i.e., not perturbatively).}

	\item 
	{At the end of the Results and Discussion section the authors present an equation for a two level model (that does not have an equation number) to estimate when dynamical corrections are large and when not. 
	This is very interesting! 
	Can more be said than dynamical effects depend on the wave function overlap between the occupied and the unoccupied state? 
	For example, would dynamical effects be larger in systems with more screening? 
	Or are they more pronounced, if $W$ has more structure in its frequency dependence? 
}
	\\
	\alert{
	We thank the referee for his/her positive comment. 
	This equation now has a number.
	We indeed discussed the most obvious cases (CT and Rydberg excitations) for which the dynamical correction must be small but could not find convincing general arguments concerning i) the matrix elements associated with e.g. $\pi \to \pi^*$ versus $n \to \pi^*$ transitions, nor ii) with the dynamical part (energy denominators) of the correction. 
	We prefer to keep our discussion as it is and avoid being too speculative at that stage.}
	
\end{itemize}
 
\end{letter}
\end{document}






